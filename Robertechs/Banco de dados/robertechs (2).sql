-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01-Out-2019 às 19:38
-- Versão do servidor: 5.7.10-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `robertechs`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `biblioteca`
--

CREATE TABLE IF NOT EXISTS `biblioteca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastra_biblio`
--

CREATE TABLE IF NOT EXISTS `cadastra_biblio` (
  `cpf` int(9) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `titulo` varchar(120) NOT NULL,
  `descricao` text NOT NULL,
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario_usuario`
--

CREATE TABLE IF NOT EXISTS `comentario_usuario` (
  `codigo` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro`
--

CREATE TABLE IF NOT EXISTS `livro` (
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(80) NOT NULL,
  `descricao` text NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `quantidade` int(20) NOT NULL,
  `autor` varchar(80) NOT NULL,
  `isbn` char(13) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `livro`
--

INSERT INTO `livro` (`titulo`, `imagem`, `descricao`, `tipo`, `quantidade`, `autor`, `isbn`) VALUES
('O patinho feio', 'patinho', 'Um patinho nasce muito diferente dos irmãos. Todos o achavam feio, por ser muito esquisito e desengonçado. Um dia, cansado daquela situação, o patinho foge para bem longe e passa por vários apuros. E, quando chega o verão, descobre sua verdadeira origem!...', '', 1, 'Bellinghausen,Ingrid Biesemeyer', '853680064'),
('\r\nDisney - Clássicos Ilustrados - Moana', 'moana', '“Moana” é uma jovem da ilha Motonui no Pacífico, que tem uma grande paixão pelo mar. Um dia, ela recebe a ousada missão de devolver o coração de Te Fiti, a ilha mãe. E vai precisar da ajuda de Maui, um atrevido e trapaceiro semideus que gosta muito de se gabar de seus feitos.Será que Maui vai acreditar na missão de “Moana”? Conseguirá ela devolver o coração de Te Fiti? Viaje pelas páginas deste livro e descubra como termina esta divertida e emocionante aventura.', 'Infantil', 11, 'Disney', '9788539418183'),
('Malala, A Menina Que Queria Ir Para A Escola\r\n', 'malala', 'No primeiro livro-reportagem destinado ao público infantil, a jornalista Adriana Carranca relata às crianças a história da adolescente paquistanesa Malala Yousafzai, baleada por membros do Talibã aos catorze anos por defender a educação feminina. Na obra, a repórter traz suas percepções sobre o vale do Swat, a história da região e a definição dos termos mais importantes para entender a vida desta menina tão corajosa.', 'Infantil', 10, 'Adriana Carranca Corrêa\r\n', '9788574066707'),
('Eu Sei o Que Vocês Fizeram No Verão Passado\r\n', 'eusei', 'Após uma festa, Julie, Helen Ray e Barry se envolvem em um acidente que termina com a morte de uma criança de dez anos. Com medo das conseqüências, os quatro jovens fazem um pacto: não contar a ninguém o que aconteceu naquela noite.Agora, um ano depois, quando Julie recebe a carta de aceitação da Universidade Smith, recebe também outro envelope, e o conteúdo se revela perturbador. Alguém sabe o que eles fizeram no verão passado. Um a um, eles começam a receber pequenos lembretes daquela noite fatídica: Helen, um anúncio de revista com uma criança andando de bicicleta, e Ray, recortes de jornal da época do acidente.', 'terror', 9, 'Ducan, Lois', '9788582401422');

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro_biblio`
--

CREATE TABLE IF NOT EXISTS `livro_biblio` (
  `isbn` char(13) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `isbn` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro_usuario`
--

CREATE TABLE IF NOT EXISTS `livro_usuario` (
  `isbn` char(13) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`isbn`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `multas`
--

CREATE TABLE IF NOT EXISTS `multas` (
  `data` date NOT NULL,
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `valor` float NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `multa_usuario`
--

CREATE TABLE IF NOT EXISTS `multa_usuario` (
  `codigo` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `telefone` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  `rg` int(10) NOT NULL,
  `bairro` varchar(120) NOT NULL,
  `senha` varchar(80) NOT NULL,
  `cep` int(80) NOT NULL,
  `tipo` char(1) NOT NULL,
  `uf` char(2) NOT NULL,
  `cidade` varchar(120) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `email` varchar(120) NOT NULL,
  `rua` varchar(120) NOT NULL,
  PRIMARY KEY (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`telefone`, `cpf`, `rg`, `bairro`, `senha`, `cep`, `tipo`, `uf`, `cidade`, `nome`, `email`, `rua`) VALUES
(212121212, 111111111, 1111111111, 'sdsdsdsdsd', 'sdsdsdsdsd', 111111, 'd', 'ds', 'sdsdsdsd', 'sdsdsdsdsdsd', 'sdsdsdsdsdsd', 'sdsdsd');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cadastra_biblio`
--
ALTER TABLE `cadastra_biblio`
  ADD CONSTRAINT `cadastra_biblio_ibfk_1` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE,
  ADD CONSTRAINT `cadastra_biblio_ibfk_2` FOREIGN KEY (`id`) REFERENCES `biblioteca` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `comentario_usuario`
--
ALTER TABLE `comentario_usuario`
  ADD CONSTRAINT `comentario_usuario_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `comentario` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comentario_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `livro_biblio`
--
ALTER TABLE `livro_biblio`
  ADD CONSTRAINT `livro_biblio_ibfk_1` FOREIGN KEY (`isbn`) REFERENCES `livro` (`isbn`) ON UPDATE CASCADE,
  ADD CONSTRAINT `livro_biblio_ibfk_2` FOREIGN KEY (`id`) REFERENCES `biblioteca` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `livro_usuario`
--
ALTER TABLE `livro_usuario`
  ADD CONSTRAINT `livro_usuario_ibfk_1` FOREIGN KEY (`isbn`) REFERENCES `livro` (`isbn`) ON UPDATE CASCADE,
  ADD CONSTRAINT `livro_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `multa_usuario`
--
ALTER TABLE `multa_usuario`
  ADD CONSTRAINT `multa_usuario_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `multas` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `multa_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
