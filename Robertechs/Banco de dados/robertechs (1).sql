-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Out-2019 às 15:08
-- Versão do servidor: 5.7.10-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `robertechs`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `biblioteca`
--

CREATE TABLE IF NOT EXISTS `biblioteca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastra_biblio`
--

CREATE TABLE IF NOT EXISTS `cadastra_biblio` (
  `cpf` int(9) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `titulo` varchar(120) NOT NULL,
  `descricao` text NOT NULL,
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario_usuario`
--

CREATE TABLE IF NOT EXISTS `comentario_usuario` (
  `codigo` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro`
--

CREATE TABLE IF NOT EXISTS `livro` (
  `titulo` varchar(80) NOT NULL,
  `imagem` varchar(80) NOT NULL,
  `descricao` text NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `quantidade` int(20) NOT NULL,
  `autor` varchar(80) NOT NULL,
  `isbn` char(13) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `livro`
--

INSERT INTO `livro` (`titulo`, `imagem`, `descricao`, `tipo`, `quantidade`, `autor`, `isbn`) VALUES
('O patinho feio', 'patinho', 'Um patinho nasce muito diferente dos irmãos. Todos o achavam feio, por ser muito esquisito e desengonçado. Um dia, cansado daquela situação, o patinho foge para bem longe e passa por vários apuros. E, quando chega o verão, descobre sua verdadeira origem!...', '', 1, 'Bellinghausen,Ingrid Biesemeyer', '853680064'),
('Dom Casmurro\r\n', 'dom', 'Machado de Assis é um dos mais importantes escritores da literatura brasileira. "Dom Casmurro" é uma de suas obras-primas. O livro apresenta o relato de Bentinho, que se crê traído pela mulher, Capitu, e pelo seu melhor amigo. É com orgulho que esta editora oferece aos seus leitores, em formato de bolso, este romance machadiano.', 'literatura', 15, 'Assis,Machado de\r\n', '8572322647'),
('Good Omens: Belas Maldições\r\n', 'good', 'Obra essencial de dois dos maiores autores britânicos de todos os tempos, agora uma aclamada série produzida pela Amazon Prime Video.O mundo vai acabar em um sábado. No próximo sábado, para falar a verdade. Pouco antes da hora do jantar. Não há nada que possa ser feito para frustrar o Grande Plano divino. Mas quando uma freira satanista um tanto distraída estraga um esquema de troca de bebês e o pequeno Anticristo acaba sendo entregue ao casal errado, tem início uma série de erros cômicos que podem ameaçar o próprio Armagedom.Aziraphale é um anjo que atua na Inglaterra e dono de um sebo nas horas vagas. Crowley é um demônio e ex-serpente responsável pela mesma região. Ambos veem nessa confusão uma grande oportunidade, porque os dois, que vivem entre os humanos desde o Princípio, apegaram-se demais ao mundo para desejar a grande batalha entre o Céu e o Inferno.', 'ficcao', 9, 'Pratchett,Terry, Gaiman,Neil\r\n', '9788528624021'),
('Jogos de Poder - Métodos Simpáticos Para Influenciar As Pessoas\r\n', 'jogospoder', 'Este livro inclinará a balança ao seu favor. Não importa se você for vendedor, advogado, garçom, professor, cuidador, gerente estratégico, estudante ou encantador de cães, a meta é ajudá-lo a dominar a arte de conseguir o que quer, e não o que os outros querem. Deixe-os envolvidos em aulas e pesquisas. Atividades assim podem ser interessantes e divertidas, mas não são realmente necessárias. Mais fácil é parar de ser um seguidor e tornar-se um líder.', 'jogos', 14, 'Fexeus,Henrik\r\n', '9788532652850'),
('O Jogo Da Amarelinha\r\n', 'ojogo', 'Tão radical quanto inclassificável, a obra-prima de Julio Cortázar mudou para sempre a história da literatura — e chega agora em nova edição ao leitor brasileiro. “A verdade, a triste ou bela verdade, é que cada vez gosto menos de romances, da arte romanesca tal como é praticada nestes tempos. O que estou escrevendo agora será (se algum dia eu terminar) algo assim como um antirromance, uma tentativa de romper os moldes em que esse gênero está petrificado”, escreveu Julio Cortázar numa carta de 1959, quando iniciava a escrita do que viria a ser O jogo da amarelinha. Publicado em 1963, o relato de amor entre um intelectual argentino no exílio, Horacio Oliveira, e uma misteriosa uruguaia, a Maga, ao acaso das ruas e das pontes de Paris, é um marco da literatura do século vinte. ', 'jogos', 10, 'Cortázar,Julio\r\n', '9788535932188'),
('Memórias Póstumas de Brás Cubas ', 'memorias', '“Não tive filhos, não transmiti a nenhuma criatura o legado da nossa miséria. ” Com essas palavras, o narrador de Memórias Póstumas de Brás Cubas resume a sua vida. O tom assumido na obra, bem como as técnicas empregadas na composição romanesca, são alguns dos fatores que justificam o lugar de Machado de Assis entre os maiores escritores do século XIX. Nesse romance repleto de digressões filosóficas, o escritor se vale da posição privilegiada de Brás Cubas, que, como “defunto autor”, narra as suas desventuras e revela as contradições da sociedade brasileira do século XIX, especialmente por meio da análise aprofundada da psicologia das personagens.', 'literatura', 12, 'Assis,Machado de\r\n', '9788538076902'),
('\r\nDisney - Clássicos Ilustrados - Moana', 'moana', '“Moana” é uma jovem da ilha Motonui no Pacífico, que tem uma grande paixão pelo mar. Um dia, ela recebe a ousada missão de devolver o coração de Te Fiti, a ilha mãe. E vai precisar da ajuda de Maui, um atrevido e trapaceiro semideus que gosta muito de se gabar de seus feitos.Será que Maui vai acreditar na missão de “Moana”? Conseguirá ela devolver o coração de Te Fiti? Viaje pelas páginas deste livro e descubra como termina esta divertida e emocionante aventura.', 'infantil', 11, 'Disney', '9788539418183'),
('Para Todas As Pessoas Intensas\r\n', 'para', '"Um dia alguém vai sumir da sua vida, só porque você é intenso demais, ou porque você é simplesmente amor demais. E algumas pessoas têm medo do amor. ser intenso é ser profundo demais, imenso demais, vivo demais, e hoje em dia, as pessoas têm um medo danado disso. ser intenso é sentir o gosto, o cheiro, o toque, a textura da pele de um jeito diferente. o coração é grande demais, e às vezes isso dói também, é que ao mesmo tempo que há espaço de sobra há muita gente pequena no mundo. tenho uma mania absurda de achar que o outro vai agir da mesma maneira transparente que eu, que o outro vai se preocupar comigo do mesmo modo que me preocupo, que o outro vai querer na mesma intensidade que quero, e isso é uma droga. não sei ser pouco, não sei gostar um pouquinho e guardar pra mim o que sinto. sou intenso, sinto muito, sinto grande. sinto tanto que sempre acho que o problema está em mim por sentir demais, quando, na verdade, as pessoas que não estão prontas pra tamanha imensidão."', 'contos', 23, 'Albuquerque,Iandê\r\n', '9788542215540'),
('Poesia Que Transforma\r\n', 'poesia', 'Bráulio Bessa conquistou o Brasil com seus cordéis no programa Encontro com Fátima Bernardes.O livro inclui o poema Recomece e ilustrações do artista baiano Elano Passos.“O Bráulio mexe com nossas memórias, nossos sentimentos, faz aflorar o melhor da gente. É poesia que sai do coração. Que alegria tê-lo toda semana no meu programa!” - Fátima Bernardes“Cada palavra que sai da boca do Bráulio Bessa toca minha alma de uma forma raríssima.” - Milton Nascimento “Bráulio Bessa é um hipnotizador de palavras. Tem o coração rimado. Quando fala, o verbo venta verso.” - Fabrício Carpinejar“Gosto de comparar a poesia a um abraço, que consegue fazer um carinho na alma sem nem saber qual é a dor que você está sentindo. A poesia se adapta à sua dor. É um abraço cego e despretensioso, como quem diz: ‘Venha! Tá doendo? Pois deixe eu dar um arrocho, que vai lhe fazer bem.’”', 'poesia', 7, 'Bessa,Bráulio\r\n', '9788543105758'),
('Genealogia da Moral\r\n', 'genealogia', 'Publicada pela primeira vez em 1887, a Genealogia da Moral é uma obra indispensável para a compreensão das reflexões com as quais Nietzsche se ocupou nos seus últimos anos de vida lúcida. Distanciando-se do estilo aforismático de outros livros, o pensador buscou apresentar aqui suas ideias de forma mais organizada, contínua e sistemática, de modo a expandir, exemplificar e aprofundar uma série de conceitos anteriormente mencionados em Além de Bem e Mal, os quais também viriam a ter importância fundamental em todas as suas publicações posteriores.', 'genealogia', 4, 'Nietzsche,Friedrich\r\n', '9788544001639'),
('A Garota do Lago\r\n', 'garota', 'Summit Lake, uma pequena cidade entre montanhas, é esse tipo de lugar, bucólico e com encantadoras casas dispostas à beira de um longo trecho de água intocada. Duas semanas atrás, a estudante de direito Becca Eckersley foi brutalmente assassinada em uma dessas casas. Filha de um poderoso advogado, Becca estava no auge de sua vida. Atraída Instintivamente pela notícia, a repórter Kelsey Castle vai até a cidade para investigar o caso. ...E Logo Se Estabelece Uma Conexão Íntima Quando Um Vivo Caminha Nas Mesmas Pegadas Dos Mortos...E enquanto descobre sobre as amizades de Becca, sua vida amorosa e os segredos que ela guardava, a repórter fica cada vez mais convencida de que a verdade sobre o que aconteceu com Becca pode ser a chave para superar as marcas sombrias de seu próprio passado.', 'terror', 21, 'Donlea,Charlie\r\n', '9788562409882'),
('Malala, A Menina Que Queria Ir Para A Escola\r\n', 'malala', 'No primeiro livro-reportagem destinado ao público infantil, a jornalista Adriana Carranca relata às crianças a história da adolescente paquistanesa Malala Yousafzai, baleada por membros do Talibã aos catorze anos por defender a educação feminina. Na obra, a repórter traz suas percepções sobre o vale do Swat, a história da região e a definição dos termos mais importantes para entender a vida desta menina tão corajosa.', 'infantil', 10, 'Adriana Carranca Corrêa\r\n', '9788574066707'),
('Destrua Este Diário\r\n', 'diario', 'Um diário costuma servir para anotar ideias, memórias ou registros do cotidiano. Keri Smith, ilustradora e artista canadense, inventou um tipo diferente de diário, que exige do usuário uma interação mais lúdica e inusitada. Com a proposta de estimular a criatividade e questionar convenções sobre a forma como lidamos com os objetos, Destrua este diário nos convida a rasgar páginas, rabiscar, pintar fora das linhas, manchar e até mesmo levar o livro para o banho. ', 'humor', 11, 'Smith,Keri\r\n', '9788580574166'),
('Termine Este Livro\r\n', 'termine', 'Keri Smith, autora de Destrua este diário, estava passeando por um parque quando encontrou um livro de conteúdo profundamente misterioso. As páginas, soltas e embaralhadas pelo vento, pareciam incompletas, e a capa, quase ilegível, exibia as palavras “Manual de instruções”. Diante desse material curioso, ela decidiu transferir para outra pessoa o desafio de decifrar o que há por trás dessa história estranha. E é você, leitor, quem tem a missão de completar o conteúdo da obra desconhecida. Mas não se preocupe: Smith jamais o deixaria desamparado. Antes de revelar os segredos desse estranho manual, você precisa passar por um treinamento intensivo nas artes da espionagem e da investigação. Aprenda a decodificar mensagens criptografadas, reconhecer padrões ocultos no ambiente e usar a criatividade para dar a objetos comuns utilidades extraordinárias. Mais que um meio de estimular a imaginação, “Termine Este Livro” é uma reflexão delicada sobre a interação entre o leitor e a obra e como os livros se entrelaçam com nossas vidas.', 'humor', 7, 'Smith, Keri', '9788580575729'),
('Eu Sei o Que Vocês Fizeram No Verão Passado\r\n', 'eusei', 'Após uma festa, Julie, Helen Ray e Barry se envolvem em um acidente que termina com a morte de uma criança de dez anos. Com medo das conseqüências, os quatro jovens fazem um pacto: não contar a ninguém o que aconteceu naquela noite.Agora, um ano depois, quando Julie recebe a carta de aceitação da Universidade Smith, recebe também outro envelope, e o conteúdo se revela perturbador. Alguém sabe o que eles fizeram no verão passado. Um a um, eles começam a receber pequenos lembretes daquela noite fatídica: Helen, um anúncio de revista com uma criança andando de bicicleta, e Ray, recortes de jornal da época do acidente.', 'terror', 9, 'Ducan, Lois', '9788582401422');

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro_biblio`
--

CREATE TABLE IF NOT EXISTS `livro_biblio` (
  `isbn` char(13) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `isbn` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `livro_usuario`
--

CREATE TABLE IF NOT EXISTS `livro_usuario` (
  `isbn` char(13) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`isbn`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `multas`
--

CREATE TABLE IF NOT EXISTS `multas` (
  `data` date NOT NULL,
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `valor` float NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `multa_usuario`
--

CREATE TABLE IF NOT EXISTS `multa_usuario` (
  `codigo` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `cpf` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `telefone` int(11) NOT NULL,
  `cpf` int(9) NOT NULL,
  `rg` int(10) NOT NULL,
  `bairro` varchar(120) NOT NULL,
  `senha` varchar(80) NOT NULL,
  `cep` int(80) NOT NULL,
  `tipo` char(1) NOT NULL,
  `uf` char(2) NOT NULL,
  `cidade` varchar(120) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `email` varchar(120) NOT NULL,
  `rua` varchar(120) NOT NULL,
  PRIMARY KEY (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`telefone`, `cpf`, `rg`, `bairro`, `senha`, `cep`, `tipo`, `uf`, `cidade`, `nome`, `email`, `rua`) VALUES
(212121212, 111111111, 1111111111, 'sdsdsdsdsd', 'sdsdsdsdsd', 111111, 'd', 'ds', 'sdsdsdsd', 'sdsdsdsdsdsd', 'sdsdsdsdsdsd', 'sdsdsd');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cadastra_biblio`
--
ALTER TABLE `cadastra_biblio`
  ADD CONSTRAINT `cadastra_biblio_ibfk_1` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE,
  ADD CONSTRAINT `cadastra_biblio_ibfk_2` FOREIGN KEY (`id`) REFERENCES `biblioteca` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `comentario_usuario`
--
ALTER TABLE `comentario_usuario`
  ADD CONSTRAINT `comentario_usuario_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `comentario` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comentario_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `livro_biblio`
--
ALTER TABLE `livro_biblio`
  ADD CONSTRAINT `livro_biblio_ibfk_1` FOREIGN KEY (`isbn`) REFERENCES `livro` (`isbn`) ON UPDATE CASCADE,
  ADD CONSTRAINT `livro_biblio_ibfk_2` FOREIGN KEY (`id`) REFERENCES `biblioteca` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `livro_usuario`
--
ALTER TABLE `livro_usuario`
  ADD CONSTRAINT `livro_usuario_ibfk_1` FOREIGN KEY (`isbn`) REFERENCES `livro` (`isbn`) ON UPDATE CASCADE,
  ADD CONSTRAINT `livro_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `multa_usuario`
--
ALTER TABLE `multa_usuario`
  ADD CONSTRAINT `multa_usuario_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `multas` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `multa_usuario_ibfk_2` FOREIGN KEY (`cpf`) REFERENCES `usuario` (`cpf`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
