<?php
  session_start();
 $name_site = "Robertechs"; 
include('components/conexao.php')
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title><?php echo $name_site; ?></title>


  </head>
  <body>


    <div class="container">
      
      <div class="row">
        <div class="col-md-3">
          <a href="./"><img class="py-2" src="./img/logo.png" width= '350' height='250'></a>
        </div>
        <div class="col mt-5">
          <h1 id="titulo">Robertechs</h1>
          <p id="texto">Cultivando a imaginação.</p>
        </div>
      </div>

        <nav id="menu">
          <ul><h2>Modelos disponíveis:</h2>
            <li><a  href="index.php">Home</a>|</li>
            <li><a  href="login.php">Login</a>|</li>
            <li><a  href="cadastro.php">Cadastrar-se</a>|</li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="ltipos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Categorias
            </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="infantil.php">Infantis</a>
                <a class="dropdown-item" href="humor.php">Humor</a>
                <a class="dropdown-item" href="jogos.php">Jogos</a>
                <a class="dropdown-item" href="literatura.php">Literatura Brasileira</a>
                <a class="dropdown-item" href="poesia.php">Poesia</a>
                <a class="dropdown-item" href="ficcao.php">Ficcao Cientifica</a>
                <a class="dropdown-item" href="genealogia.php">Geneologia</a>
                <a class="dropdown-item" href="contos.php">Contos</a>
                <a class="dropdown-item" href="terror.php">Terror</a>

              </div>
            </li>
          </ul>
        </nav>

      <div class="row pt-3">
        <div class="col-md-9">
        </body>
        </html>
          <style type="text/css">

      /*
      .btn-danger{
        background-color: #0000FF;
      }
      .btn-danger a:hover{
        background-color: #00FF00;
      }]
      */
      #titulo{
        font-size: 100px;
        color: #2E2E2E;
      }

      #texto{
        font-size: 25px;
        color: #2E2E2E;
      }

      #ltipos{
        color: #F5FFFA;
        background-color:#F5FFFA;
      }

      #menu ul {
          padding:0px;
          margin:0px;
          background-color:#F5FFFA;
          list-style:none;
      }
      #menu ul li { display: inline; }
      #menu ul li a {
          padding: 2px 10px;
          display: inline-block;
          background-color:#F5FFFA;
          color: #333;
          text-decoration: none;
          border-bottom:3px solid #F5FFFA;
      }
      #menu ul li a:hover {
          background-color:#F5FFFA;
          color: #000000;
          border-bottom:3px solid #EA0000;
        }
        body  {
                background: #F5FFFA;
            }
      #img{
        float:left;
        margin-right: 5px; 
      }
      </style>